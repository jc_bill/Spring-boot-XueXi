package com.example.lesson2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Lesson2ApplicationTests {

	@Autowired
	private User user;
	@Autowired
	private Person person;
	
	@Test
	public void userTest() {
		System.out.println("user.getName() = " + user.getName());
		System.out.println("user.getAge() = " + user.getAge());
		System.out.println("user.getDesc() = " + user.getDesc());
	}

	@Test
	public void personTest() {
		System.out.println("person.getName() = " + person.getName());
		System.out.println("person.getAge() = " + person.getAge());
		System.out.println("person.getDesc() = " + person.getDesc());
	}


}
