package com.example.lesson2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {

    @Autowired
    private User user;
    @Autowired
    private Person person;


    @GetMapping(value = "user")
    public String user(){
        return user.getDesc();
    }

    @GetMapping(value = "person")
    public String person(){
        return person.getDesc();
    }
}
