package com.example.lesson2;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "person.user")
public class User {
    private String name;
    private Integer age;
    private String desc;
}
