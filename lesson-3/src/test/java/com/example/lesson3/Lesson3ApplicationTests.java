package com.example.lesson3;

import com.example.lesson3.domain.User;
import com.example.lesson3.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Lesson3ApplicationTests {

	@Autowired
	private IUserService userService;

	@Test
	public void contextLoads() {
		for (int i = 0; i < 100; i++) {
			User user = new User();
			user.setName("test"  + i);
			user.setAge(22);
			user.setAddress("桥头");
			userService.saveUser(user);
		}
	}

}
