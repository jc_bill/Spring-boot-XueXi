package com.example.lesson3.service;

import com.example.lesson3.domain.User;

import java.util.List;

public interface IUserService {
    void saveUser(User user);
    void updateUser(User user);
    void deleteUserById(Long id);
    User getUserById(Long id);
    List<User> getUserlist();
}
