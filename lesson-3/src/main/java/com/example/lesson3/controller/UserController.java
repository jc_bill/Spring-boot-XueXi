package com.example.lesson3.controller;

import com.example.lesson3.domain.User;
import com.example.lesson3.service.IUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("get/{id}")
    public User getUser(@PathVariable Long id){
        User user = userService.getUserById(id);
        return user;
    }

    @PutMapping("update/{id}")
    public String updateUser(@PathVariable Long id, User user ){
        userService.updateUser(user);
        return "success";
    }

    @DeleteMapping("delete/{id}")
    public String deleteUser(@PathVariable Long id){
        userService.deleteUserById(id);
        return "success";
    }

    @PostMapping("save")
    public String saveUser(User user){
        userService.saveUser(user);
        return "success";
    }

    @GetMapping("list")
    public PageInfo<User> getList(@RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "20")Integer pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<User> userlist = userService.getUserlist();
        PageInfo<User> pageInfo = new PageInfo<>(userlist);
        return pageInfo;
    }
}
