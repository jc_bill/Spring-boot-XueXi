package com.example.lesson3.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel("用户实体类")
public class User {
    private Long id;
    private String name;
    private Integer age;
    private String address;
}