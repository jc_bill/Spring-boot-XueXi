package com.example.lesson3;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan("com.example.lesson3.mapper")
public class Lesson3Application {
	public static void main(String[] args) {
		SpringApplication.run(Lesson3Application.class, args);
	}
}
